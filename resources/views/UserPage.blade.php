<html>
   <head>
      <script src="/js/jquery.js"></script>
      <script>
         $(function(){
            $("#refresh").click(function(){
               $("#loader").html('<img src="images/loader.gif" />');
               setTimeout(function(){
                  $.ajax({
                     url: "http://localhost:8000/api/user",
                     success : function(res){
                        var count = res.length;
                        var html = "";
                        for(var i=0; i<count; i++){
                           html += "<tr>";
                           html += "<td>"+res[i].id+"</td>";
                           html += "<td>"+res[i].name+"</td>";
                           html += "<td>"+res[i].address+"</td>";
                           html += "</tr>";
                        }
                        $("#target-body").html(html);
                        $("#loader").html('');
                     }
                  });
               }, 2000);
            });
         });
      </script>
   </head>
   <body>
      <table id="target">
         <thead>
            <tr>
               <th>ID</th>
               <th>Name</th>
               <th>Address</th>
            </tr>
         </thead>
         <tbody id="target-body">
         @foreach ($users as $user)
            <tr>
               <td>{{ $user->id }}</td>
               <td>{{ $user->name }}</td>
               <td>{{ $user->address }}</td>
            </tr>
         @endforeach
         </tbody>
      </table>
      <br />
      <input type="button" name="btn" id="refresh" value="refresh" />
      <div id="loader"></div>
      </body>
</html>
