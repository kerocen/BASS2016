<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\UserModel;

class MyController extends Controller
{
   public function __invoke(){
      $data['users'] = UserModel::all();
      return view("UserPage", $data);
   }
}
