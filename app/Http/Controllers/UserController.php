<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\UserModel;

class UserController extends Controller
{
   public function getUsers(){
      //associative array
      $users = UserModel::all();
      return response()->json($users);
   }
   
}
