<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DefaultController extends Controller
{
   public function firstFunction(){
      $arr = Array(
         'param1' => 'Cool',
         'param2' => 'Bro'
      );
      return view('firstPage', $arr);
   }
}

